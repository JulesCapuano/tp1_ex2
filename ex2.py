# -*- coding: utf-8 -*-
"""
Éditeur de Spyder

Ceci est un script temporaire.
"""

class SimpleCalculator:
  def __init__(self,nbr1,nbr2):
    self.nbr1 = nbr1
    self.nbr2 = nbr2

  def sum(self):
    return(self.nbr1 + self.nbr2)
  
  def substract(self):
    return(self.nbr1 - self.nbr2)

  def multiply(self):
    return(self.nbr1 * self.nbr2)

  def divide(self):
    return(self.nbr1 / self.nbr2)


somme =SimpleCalculator(1,3)

print(somme.sum())
print(somme.substract())
print(somme.multiply())
print(somme.divide())
    
